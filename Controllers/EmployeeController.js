const Employee = require('../models/EmployeeModel')

// controllers/employeeController.js


exports.registerEmployee = async (req, res) => {
  try {
    const {
      name,
      employeeId,
      gender,
      dob,
      designation,
      department,
      appointmentDate,
    } = req.body;

    // Create a new employee instance
    const newEmployee = new Employee( req.body);

    // Save the employee to the database
    await newEmployee.save();

    res.json(newEmployee); // Return the created employee as a response
  } catch (error) {
    console.error('Error registering employee:', error);
    res.status(500).json({ error: 'An error occurred while registering employee' });
  }
};

exports.getAllEmployee = async (req, res, next) =>{
    try{
      const{page =1,limit = 5} = req.query;

        const employee = await Employee.find().limit(limit*1).skip((page-1)*limit)
        
        res.status(200).json({data: employee, status: 'success'})
    }catch(err){
        res.status(500).json({error: err.message});
    }
}

exports.updateEmployee = async (req, res) => {
  try {
    const employee = await Employee.findByIdAndUpdate(req.params.id, req.body, { new: true });

    if (!employee) {
      return res.status(404).json({ error: 'Employee not found' });
    }

    return res.json({ data: employee, status: 'success' });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.deleteEmployee = async (req, res) =>{
  try {
      const employee = await Employee.findByIdAndDelete(req.params.id);
      res.json({ data: employee, status: 'success' });
  } catch (err) {
      res.status(500).json({ error: err.message });
  }
}


exports.paginationEmployee = async (req, res, next) => {
    try {
      const page = parseInt(req.query.page) || 1; // Get the page number from the query string, default to 1
      const pageSize = parseInt(req.query.pageSize) || 10; // Get the page size from the query string, default to 10
  
      const skip = (page - 1) * pageSize; // Calculate the number of records to skip
  
      const totalEmployees = await Employee.countDocuments(); // Get the total number of employees
      const totalPages = Math.ceil(totalEmployees / pageSize); // Calculate the total number of pages
  
      const employees = await Employee.find()
        .skip(skip)
        .limit(5)
        .exec();
  
      res.status(200).json({
        data: employees,
        page,
        pageSize,
        totalPages,
        totalEmployees,
        status: 'success',
      });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };

  exports.sortingEmployee = async (req, res, next) => {
    try {
      const employees = await Employee.find().sort({ name: 'asc' }); // Use 'asc' for ascending order
      res.status(200).json({
        data: employees,
        status: 'success',
      });
    } catch (error) {
      console.log(error);
    }
  };
  
  
  

  
exports.searchEmployee = async (req, res, next) => {
  try {
    const searchName = req.query.searchName; // Get the search name from the query string

    let query = Employee.find(); // Start with a basic query to retrieve all employees

    if (searchName) {
      query = query.where('name', new RegExp(searchName, 'i')); // Add a condition to search by name (case-insensitive)
    }

    const employees = await query.exec();

    res.status(200).json({
      data: employees,
      status: 'success',
    });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
  