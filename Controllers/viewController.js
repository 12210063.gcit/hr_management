const path = require('path')

/* Landing PAGE */

exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'main.html'))
}


exports.getForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'form.html'))
}
exports.getLogin = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'login.html'))
}

exports.getmain = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'home.html'))
}

exports.getResult = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'result.html'))
}
exports.updateEmployee = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'update-form.html'))
}




